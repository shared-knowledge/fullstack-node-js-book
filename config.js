require('dotenv').config()

module.exports = {
    adminPassword: process.env.ADMIN_PASSWORD || 'Ubdw=46+6N__txN&cv6d^g&tG6KwXpXq',
    jwtSecret: process.env.JWT_SECRET || 'Tq*g^Bvpb^SNH6Rcy%9Y39g5@k=K-825',
    mongo: {
        connectionString: process.env.MONGO_URI || 'mongodb://localhost:27017/printshop'
    },
    port: process.env.PORT || 1337
}