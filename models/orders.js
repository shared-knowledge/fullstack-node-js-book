const cuid = require('cuid')
const { isEmail } = require('validator')

const db = require('../db')

module.exports = {
    get,
    list,
    create
}

async function get (_id) {
    const order = await Order.findById(_id)
    .populate('products')
    .exec()
}

async function list (opts = {}) {
    const { 
        offset = 0, 
        limit = 25,
        username,
        productId,
        status
    } = opts

    const query = {}
    if (username) query.username = username
    if (productId) query.products = productId
    if (status) query.status = status

    const orders = Order.find(query)
    .sort({ _id: 1 })
    .skip(offset)
    .limit(limit)
    .populate('products')
    .exec()

    return orders
}

async function create (fields) {
    const order = await new Order(fields).save()
    await order.populate('products').execPopulate()
    return order
}

const Order = db.model('Order', {
    _id: { type: String, default: cuid},
    username: { type: String, required: true, index: true },
    products: [
        { 
            type: String, 
            ref: 'Product', 
            index: true, 
            required: true 
        }
    ],
    status: { 
        type: String, 
        index: true, 
        default: 'CREATED', 
        enum: ['CREATED', 'PENDING', 'COMPLETED']
    }
})