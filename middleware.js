const pinoNoir = require('pino-noir')
const { STATUS_CODES } = require('http')
const pinoLogger = require('express-pino-logger')

module.exports = {
    cors,
    notFound,
    handleError,
    logger: logger(),
    handleValidationError
}

function logger () {
    return pinoLogger({
        serializers: pinoNoir([
            'req.headers.cookie',
            'res.headers.set-cookie',
            'req.headers.authorization'
        ])
    })
}

function cors (req, res, next) {
    const origin = req.headers.origin

    res.setHeader('Access-Control-Allow-Origin', origin || '*')
    res.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS, XMODIFY')
    res.setHeader('Access-Control-Allow-Credentials', 'true')
    res.setHeader('Access-Control-Max-Age', '86400')
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept')

    next()
}

function handleError (error, req, res, next) {
    console.error(error)
    if (res.headersSent) return next(error)

    const statusCode = error.statusCode || 500
    const errorMessage = STATUS_CODES[statusCode] || 'Internal Error'
    
    res.status(statusCode).json({ error: errorMessage })
}

function notFound (req, res) {
    res.status(404).json({ error: 'Not Found' })
}

function handleValidationError (error, req, res, next) {
    if (error.name !== 'ValidationError') return next(error)

    res.status(400).json({ error: error._message, errorDetails: error.errors })
}