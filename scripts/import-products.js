const db = require('../db')
const Products = require('../models/products')

const products = require('../../products.json')

;(async function () {
    for (let i = 0; i < products.length; i++) {
        console.log(`========== creating ${products[i]} ===============`)
        if (products[i]) await Products.create(products[i])
        console.log(`====================done==========================`)
    }
    db.disconnect()
})()