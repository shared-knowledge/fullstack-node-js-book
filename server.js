const config = require('./config')
const hpp = require('hpp')

const express = require('express')
const cookieParser = require('cookie-parser')

const auth = require('./auth')
const api = require('./api')
const middleware = require('./middleware')

const port = config.port

const app = express()

app.disable('x-powered-by')

app.use(hpp())
app.use(middleware.logger)
app.use(middleware.cors)
app.use(express.json())
app.use(cookieParser())

app.get('/health', api.health)

app.post('/login', auth.authenticate, auth.login)

app.post('/users', api.createUser)

app.get('/orders', auth.ensureUser, api.listOrders)
app.post('/orders', auth.ensureUser, api.createOrder)

app.get('/products', api.listProducts)
app.get('/products/:id', api.getProduct)
app.post('/products', auth.ensureUser, api.createProduct)
app.put('/products/:id', auth.ensureUser, api.editProduct)
app.delete('/products/:id', auth.ensureUser, api.deleteProduct)

app.use(middleware.handleValidationError)
app.use(middleware.handleError)
app.use(middleware.notFound)

app.listen(port, () => console.log(`Server listening on port ${port}`))